<?php
use Symfony\Component\HttpFoundation\Request;
use CMS\Domain\Chapter;
use CMS\Domain\Comment;
use CMS\Domain\User;
use CMS\Form\Type\ChapterType;
use CMS\Form\Type\CommentType;
use CMS\Form\Type\UserType;


// Home page
$app->get('/', function () use ($app)
{
    $chapters = $app['dao.chapter']->getLastChapters();

    return $app['twig']->render(
        'index.html.twig',
        array(
            'chapters' => $chapters
        )
    );
})->bind('home');

// Chapter details with comments
$app->match('/chapter/{id}', function ($id, Request $request) use ($app) {
    $chapter = $app['dao.chapter']->find($id);

    $commentFormView = null;
    if ($app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')) {
        // A user is fully authenticated : he can add comments
        $comment = new Comment();
        $comment->setChapter($chapter);

        $user = $app['user'];
        $comment->setAuthor($user);

        $commentForm = $app['form.factory']->create(CommentType::class, $comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $app['dao.comment']->save($comment);
            $app['session']->getFlashBag()->add('success', 'Your comment was successfully added.');
        }
        $commentFormView = $commentForm->createView();
    }
    $comments = $app['dao.comment']->findAllByChapter($id);

    return $app['twig']->render(
            'chapter.html.twig',
            array(
                'chapter' => $chapter,
                'comments' => $comments,
                'commentForm' => $commentFormView
            ));
})->bind('chapter');

// Login form
$app->get('/login', function(Request $request) use ($app) {
    return $app['twig']->render(
        'login.html.twig',
        array(
            'error'         => $app['security.last_error']($request),
            'last_username' => $app['session']->get('_security.last_username'),
        )
    );
})->bind('login');




/*  ##################################################### */
/** #                   ADMIN ROUTE                       */
/*  ##################################################### */

// Admin home page
$app->get('/admin', function() use ($app) {
    $chapters   = $app['dao.chapter']->findAll();
    $comments   = $app['dao.comment']->findAll();
    $users      = $app['dao.user']->findAll();

    return $app['twig']->render('admin.html.twig', array(
        'chapters' => $chapters,
        'comments' => $comments,
        'users' => $users));
})->bind('admin');



// CHAPTER //

// Add a new chapter
$app->match('/admin/chapter/add', function(Request $request) use ($app) {
    $chapter = new Chapter();
    $chapterForm = $app['form.factory']->create(ChapterType::class, $chapter);
    $chapterForm->handleRequest($request);
    if ($chapterForm->isSubmitted() && $chapterForm->isValid()) {
        $app['dao.chapter']->save($chapter);
        $app['session']->getFlashBag()->add('success', 'The chapter was successfully created.');
    }
    return $app['twig']->render('chapter_form.html.twig', array(
        'title' => 'New chapter',
        'chapterForm' => $chapterForm->createView()));
})->bind('admin_chapter_add');

// Edit an existing chapter
$app->match('/admin/chapter/{id}/edit', function($id, Request $request) use ($app) {
    $chapter = $app['dao.chapter']->find($id);
    $chapterForm = $app['form.factory']->create(ChapterType::class, $chapter);
    $chapterForm->handleRequest($request);
    if ($chapterForm->isSubmitted() && $chapterForm->isValid()) {
        $app['dao.chapter']->save($chapter);
        $app['session']->getFlashBag()->add('success', 'The article was successfully updated.');
    }
    return $app['twig']->render('chapter_form.html.twig', array(
        'title' => 'Edit chapter',
        'chapterForm' => $chapterForm->createView()));
})->bind('admin_chapter_edit');

// Remove an chapter
$app->get('/admin/chapter/{id}/delete', function($id, Request $request) use ($app) {
    // Delete all associated comments
    $app['dao.comment']->deleteAllByChapter($id);
    // Delete the chapter
    $app['dao.chapter']->delete($id);
    $app['session']->getFlashBag()->add('success', 'The chapter was successfully removed.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_chapter_delete');




// COMMENT //

// Edit an existing comment
$app->match('/admin/comment/{id}/edit', function($id, Request $request) use ($app) {
    $comment = $app['dao.comment']->find($id);
    $commentForm = $app['form.factory']->create(CommentType::class, $comment);
    $commentForm->handleRequest($request);
    if ($commentForm->isSubmitted() && $commentForm->isValid()) {
        $app['dao.comment']->save($comment);
        $app['session']->getFlashBag()->add('success', 'The comment was successfully updated.');
    }
    return $app['twig']->render('comment_form.html.twig', array(
        'title' => 'Edit comment',
        'commentForm' => $commentForm->createView()));
})->bind('admin_comment_edit');

// Remove a comment
$app->get('/admin/comment/{id}/delete', function($id, Request $request) use ($app) {
    $app['dao.comment']->delete($id);
    $app['session']->getFlashBag()->add('success', 'The comment was successfully removed.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_comment_delete');




// USER //

// Add a user
$app->match('/admin/user/add', function(Request $request) use ($app) {
    $user = new User();
    $userForm = $app['form.factory']->create(UserType::class, $user);
    $userForm->handleRequest($request);

    if ($userForm->isSubmitted() && $userForm->isValid()) {
        // generate a random salt value
        $salt = substr(md5(time()), 0, 23);
        $user->setSalt($salt);
        $plainPassword = $user->getPassword();
        // find the default encoder
        $encoder = $app['security.encoder.bcrypt'];
        // compute the encoded password
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setPassword($password);
        $app['dao.user']->save($user);
        $app['session']->getFlashBag()->add('success', 'The user was successfully created.');
    }

    return $app['twig']->render('user_form.html.twig', array(
        'title' => 'New user',
        'userForm' => $userForm->createView()));
})->bind('admin_user_add');

// Edit an existing user
$app->match('/admin/user/{id}/edit', function($id, Request $request) use ($app) {
    $user = $app['dao.user']->find($id);
    $userForm = $app['form.factory']->create(UserType::class, $user);
    $userForm->handleRequest($request);

    if ($userForm->isSubmitted() && $userForm->isValid()) {
        $plainPassword = $user->getPassword();
        // find the encoder for the user
        $encoder = $app['security.encoder_factory']->getEncoder($user);
        // compute the encoded password
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setPassword($password);
        $app['dao.user']->save($user);
        $app['session']->getFlashBag()->add('success', 'The user was successfully updated.');
    }
    return $app['twig']->render('user_form.html.twig', array(
        'title' => 'Edit user',
        'userForm' => $userForm->createView()));
})->bind('admin_user_edit');

// Remove a user
$app->get('/admin/user/{id}/delete', function($id, Request $request) use ($app) {
    // Delete all associated comments
    $app['dao.comment']->deleteAllByUser($id);
    // Delete the user
    $app['dao.user']->delete($id);
    $app['session']->getFlashBag()->add('success', 'The user was successfully removed.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_user_delete');