<?php

function getChapters() {
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=p3-new-nersion;charset=utf8', 'root', '');
        $chapters = $bdd->query('select * from t_chapter order by chap_id desc');
        return $chapters;
    } catch (Exception $e) {
        die($e->getMessage());
    }
}