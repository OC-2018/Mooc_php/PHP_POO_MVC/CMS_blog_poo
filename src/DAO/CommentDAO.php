<?php

namespace CMS\DAO;


use CMS\Domain\Comment;

class CommentDAO extends DAO
{

    /**
     * @var \CMS\DAO\ChapterDAO
     */
    protected $chapterDAO;

    /**
     * @var \CMS\DAO\UserDAO
     */
    protected $userDAO;

    public function getChapterDAO()  { return $this->chapterDAO; }
    public function getUserDAO()        { return $this->userDAO; }

    public function setChapterDAO(ChapterDAO $chapterDAO)   { $this->chapterDAO = $chapterDAO; }
    public function setUserDAO($userDAO)                    { $this->userDAO = $userDAO;}

    /**
     * Returns a comment matching the supplied id.
     *
     * @param integer $id The comment id
     * @return \CMS\Domain\Comment|throws an exception if no matching comment is found
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function find($id) {
        $sql = "select * from t_comment where com_id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("No comment matching id " . $id);
    }

    /**
     * Delete a comment
     *
     * @param $id
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete($id) {
        $this->getDb()->delete('t_comment', array('com_id' => $id));
    }

    /**
     * Return a list of all comments
     *
     * @return array $comments
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function findAll() {
        $sql = "SELECT * FROM t_comment ORDER BY com_id DESC";

        $result = $this->getDb()->fetchAll($sql);

        $comments = array();
        foreach($result as $row) {
            $id = $row['com_id'];
            $comments[$id] = $this->buildDomainObject($row);
        }

        return $comments;
    }

    /**
     * Return a list of all comments for an chapter, sorted by date (most recent last).
     *
     * @param integer $chapterId The chapter id
     * @return array A list of all comments for the chapter.
     * @throws \Exception
     */
    public function findAllByChapter($chapterId) {
        // The associated chapter is retrieved only once
        $chapter = $this->chapterDAO->find($chapterId);

        // chap_id is not selected by the SQL query
        // The chapter won't be retrieved during domain objet construction
        $sql = "SELECT * FROM t_comment WHERE com_chapter_id = ? ORDER BY com_id";

        $result = $this->getDb()->fetchAll($sql, array($chapterId));

        // Convert query result to an array of domain objects
        $comments = array();

        foreach ($result as $row) {
            $comId = $row['com_id'];
            $comment = $this->buildDomainObject($row);

            // The associated chapter is defined for the constructed comment
            $comment->setChapter($chapter);
            $comments[$comId] = $comment;
        }

        return $comments;
    }

    /**
     * Saves a comment into the database
     *
     * @param Comment $comment
     * @throws \Doctrine\DBAL\DBALException
     */
    public function save(Comment $comment) {
        $data = array(
            'com_chapter_id'    => $comment->getChapter()->getId(),
            'com_author_usr_id' => $comment->getAuthor()->getId(),
            'com_content'       => $comment->getContent()
        );

        if($comment->getId()) {
            // The comment has already been saved: UPDATE IT
            $this->getDb()->update('t_comment', $data, array('com_id' => $comment->getId() ));
        } else {
            // The comment has never been saved: INSERT IT
            $this->getDb()->insert('t_comment', $data);
            //Get the id of the newly created comment and set it on the entity
            $id = $this->getDb()->lastInsertId();
            $comment->setId($id);
        }
    }

    /**
     * Delete all Comments By chapter Id
     *
     * @param $chapId
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function deleteAllByArticle($chapId) {
        $this->getDb()->delete('t_comment', array('com_chapter_id' => $chapId));
    }

    /**
     * Dellet all comments by user
     *
     * @param $userId
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function deleteAllByUser($userId) {
        $this->getDb()->delete('t_comment', array('usr_id' => $userId));
    }

    /**
     * Build a comment Object
     *
     * @param array $row
     * @return Comment
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    protected function buildDomainObject(array $row) {
        $comment = new Comment();
        $comment->setId($row['com_id']);
        $comment->setContent($row['com_content']);

        if (array_key_exists('com_chapter_id', $row)) {
            // Find and set the associated article
            $chapId = $row['com_chapter_id'];
            $chapter  = $this->chapterDAO->find($chapId);
            $comment->setChapter($chapter);
        }
        if (array_key_exists('com_author_usr_id', $row)) {
            // Find and set the associated author
            $userId = $row['com_author_usr_id'];
            $user = $this->userDAO->find($userId);
            $comment->setAuthor($user);
        }
        
        return $comment;
    }
}