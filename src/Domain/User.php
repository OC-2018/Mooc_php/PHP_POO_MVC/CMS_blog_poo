<?php

namespace CMS\Domain;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /**
     * User id.
     *
     * @var integer
     */
    private $id;
    /**
     * User name.
     *
     * @var string
     */
    private $username;
    /**
     * User firstname
     * @var string
     */
    private $firstname;
    /**
     * User lastname
     * @var string
     */
    private $lastname;
    /**
     * User email
     * @var string
     */
    private $email;
    /**
     * User password.
     *
     * @var string
     */
    private $password;
    /**
     * Salt that was originally used to encode the password.
     *
     * @var string
     */
    private $salt;
    /**
     * Role.
     * Values : ROLE_USER or ROLE_ADMIN.
     *
     * @var string
     */
    private $role;
    /**
     * @var \DateTime
     */
    private $createdAt;
    /**
     * @var \DateTime
     */
    private $modifiedAt;

    public function getId()         { return $this->id; }
    public function setId($id)      { $this->id = $id; return $this; }

    /**
     * @inheritDoc
     */
    public function getSalt()       { return $this->salt; }
    public function setSalt($salt)  { $this->salt = $salt; return $this; }

    public function getRole()       { return $this->role; }
    public function setRole($role)  { $this->role = $role; return $this; }

    /**
     * @inheritDoc
     */
    public function getUsername()           { return $this->username; }
    public function setUsername($username)  { $this->username = $username;  return $this; }

    /**
     * @inheritDoc
     */
    public function getPassword()           { return $this->password; }
    public function setPassword($password)  { $this->password = $password;  return $this; }

    public function getFirstname()                  { return $this->firstname; }
    public function setFirstname(string $firstname) { $this->firstname = $firstname; return $this; }

    public function getLastname()                   { return $this->lastname; }
    public function setLastname(string $lastname)   { $this->lastname = $lastname; return $this; }

    public function getEmail()                      { return $this->email; }
    public function setEmail(string $email)         { $this->email = $email; return $this; }

    public function getCreatedAt()                      { return $this->createdAt; }
    public function setCreatedAt(\DateTime $createdAt)  { $this->createdAt = $createdAt; return $this; }

    public function getModifiedAt()                     { return $this->modifiedAt; }
    public function setModifiedAt(\DateTime $modifiedAt)  { $this->modifiedAt = $modifiedAt; return $this; }

    /**
     * @inheritDoc
     */
    public function getRoles(){ return array($this->getRole()); }

    /**
     * @inheritDoc
     */
    public function eraseCredentials() {
        // Nothing to do here
    }
}
