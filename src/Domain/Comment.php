<?php

namespace CMS\Domain;

/**
 * Class Comment
 * @package CMS\Domain
 */
class Comment 
{
    /**
     * Comment id.
     *
     * @var integer
     */
    private $id;

    /**
     * Comment content.
     *
     * @var integer
     */
    private $content;

    /**
     * @var bool
     */
    private $signaled;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * Associated Use .
     *
     * @var \CMS\Domain\User
     */
    private $author;

    /**
     * Associated chapter.
     * @var \CMS\Domain\Chapter
     */
    private $chapter;

    

    public function getId()         { return $this->id; }
    public function getAuthor()     { return $this->author; }
    public function getContent()    { return $this->content; }
    public function getChapter()    { return $this->chapter; }
    public function isSignaled()    { return $this->signaled === 0 ? false : true; }
    public function getCreatedAt()  { return $this->createdAt; }
    public function getModifiedAt() { return $this->modifiedAt; }



    public function setId(int $id)                      { $this->id       = $id;         return $this; }
    public function setContent(string $content)         { $this->content  = $content;    return $this; }
    public function setAuthor (User $author)            { $this->author   = $author;     return $this; }
    public function setChapter(Chapter $chapter)        { $this->chapter  = $chapter;    return $this; }
    public function setSignaled(bool $signaled)         { $this->signaled = $signaled;   return $this; }
    public function setCreatedAt(\DateTime $createdAt)  { $this->createdAt = $createdAt;  return $this; }
    public function setModifiedAt(\DateTime $modifiedAt){ $this->modifiedAt= $modifiedAt;  return $this; }
}
